package com.makaryk;

import com.makaryk.stateimpl.NewOrderState;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;

public class Order {
    private State state;
    private List<Product> products = new LinkedList<>();

    public Order() {
        state = new NewOrderState();
    }

    public void setState(State state) {
        this.state = state;
    }

    public void addProduct() {
        state.addProduct(this);
    }

    public void register() {
        if (products.isEmpty()) {
            System.out.println("Order is empty.");
        } else {
            state.register(this);
        }
    }

    public void grant() {
        state.grant(this);
    }

    public void invoice() {
        state.invoice(this);
    }

    public void ship() {
        state.ship(this);
    }

    public void cancel() {
        state.cancel(this);
    }

    public void addProductToList() {
        Product product = new Product();
        products.add(product);
        System.out.println("Add to Bucket");
    }
    public void getProductList(){
        System.out.println(products);
    }
}
