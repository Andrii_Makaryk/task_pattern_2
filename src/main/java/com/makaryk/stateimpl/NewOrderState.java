package com.makaryk.stateimpl;

import com.makaryk.Order;
import com.makaryk.State;

public class NewOrderState implements State {
    @Override
    public void addProduct(Order order) {
        order.addProductToList();
    }

    @Override
    public void register(Order order) {
        order.setState(new RegistratedState());
        System.out.println("Order -is register");
    }

    @Override
    public void cancel(Order order) {
        order.setState(new CancelState());
        System.out.println("Order -is canceled");
    }
}
