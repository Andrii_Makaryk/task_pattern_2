package com.makaryk.stateimpl;

import com.makaryk.Order;
import com.makaryk.State;

public class InvoicedState implements State {

    @Override
    public void ship(Order order) {
        order.setState(new ShippedState());
        System.out.println("Order is shipped");
    }
}
