package com.makaryk;

public interface State {
    default void addProduct(Order order) {
        System.out.println("addProduct -is allowed");
    }

    default void register(Order order) {
        System.out.println("register -is allowed");
    }

    default void grant(Order order) {
        System.out.println("grant -is grant");
    }

    default void invoice(Order order) {
        System.out.println("invoice -is invoice");
    }

    default void ship(Order order) {
        System.out.println("ship -is ship");
    }

    default void cancel(Order order) {
        System.out.println("cancel -is canceled");
    }


}

