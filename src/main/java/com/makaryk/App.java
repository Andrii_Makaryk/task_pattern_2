package com.makaryk;

import javafx.scene.paint.Color;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String keyMenu = null;
        Order order = new Order();
        do {

            System.out.println("   1-Add Product" + "  2-Register   ");
            System.out.println("   3-Grant      " + "  4-Invoice    ");
            System.out.println("   5-Ship       " + "  6-Cancel     ");
            System.out.println("   L-Get Product List" + "  Q-Exit");
            System.out.println("Please, select menu point.");

            keyMenu = input.nextLine().toUpperCase();
            try {
                switch (keyMenu) {
                    case "1":
                        order.addProduct();
                        break;
                    case "2":
                        order.register();
                        break;
                    case "3":
                        order.grant();
                        break;
                    case "4":
                        order.invoice();
                        break;
                    case "5":
                        order.ship();
                        break;
                    case "6":
                        order.cancel();
                        break;
                    case "L":
                        order.getProductList();
                        break;
                }
            } catch (Exception ex) {

            }


        } while (!keyMenu.equals("Q"));
        input.close();
    }
}
