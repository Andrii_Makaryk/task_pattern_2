package com.makaryk;

public class Product {
    public static int counter = 0;
    private int productNumber;

    public Product() {
        counter++;
        this.productNumber = counter;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productNumber=" + productNumber +
                '}';
    }
}
